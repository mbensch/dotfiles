# Custom Aliases
function gcm () {
  noglob git commit -m $*
}

alias vap='cd ~/Development/vipassana'
alias artisan='php artisan'
alias migrate='php artisan migrate'
